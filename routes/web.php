<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
Route::get('/', function () {
    return view('index');
});
Route::get('/', 'ProductController@index');

Route::get('/sell/{product}', 'ProductController@show')->name('product.show');

Route::post('/addToCart/{product}','HomeController@addToCart')->name('addToCart');

Route::post('/CompleteCart','HomeController@CompleteCart')->name('CompleteCart');

Route::post('/addingProduct','ProductController@addingProduct')->name('addingProduct');

Route::post('/addingCategory','categorycontroller@addingcategory')->name('addingCategory');

Route::get('/addingMassage','MassageController@addingMassage')->name('addingMassage');

Route::get('/user/Cart','HomeController@Cart')->name('Cart');

Route::get('/user/massage','HomeController@massage')->name('massage');

Route::get('/admin',function (){
    return view('admin');
});

Route::get('/user',function (){
    return view('user');
});

Route::get('/admin/addProduct','ProductController@addProduct')->name('addProduct');

Route::get('/admin/addCategory','categorycontroller@addCategory')->name('addCategory');

Route::get('/admin/massages','HomeController@massages')->name('massages');

Route::get('/admin/Answer','HomeController@Answer')->name('Answer');

Auth::routes();

Route::get('/user', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/user', 'HomeController@index')->name('home');
