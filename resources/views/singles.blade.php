@include('layouts.header1')
<body calss="wmax">
<div id="main">
    <div class="inner-wrapper">
        <section id="frmSecProductMain" class="charity clearfix available amazing-offer">
            <div class="products-gallery">
                <div class="dk-products-image">
                    <div class="thumb-image">
                        <img style="margin-top: 150px" src="" data-imagezoom="/" title="{{$product->Name}}" width="350" height="350" itemprop="image" data-magnification="4" data-zoomviewsize="[570,570]">
                    </div>
                </div>
            </div>
            <div class="products-info">
                <div id="ProductConfigAjaxProgress" class="">
                    <div>
                        <img src="https://template.digi-kala.com/digikala//Image/Public/galleryloading.gif" style="vertical-align: middle">
                        <p style="direction: rtl; opacity: 1">لطفاً چند لحظه صبر نمایید ...</p>
                    </div>
                </div>
                <header class="clearfix">
                    <div class="info-header">

                        <h1>
                           {{$product->Name}}

                            <span style="display: block;">
                {{$product->Description}}
            </span>
                        </h1>


                    </div>
                </header>

                <div id="productConfigContainer">
                    <div class="products-config clearfix">
                        <div class="config-right">

                            <input id="product_title" name="product_title" type="hidden" value="2q/ZiNi02Yog2YXZiNio2KfZitmEINiz2KfZhdiz2YjZhtqvINmF2K/ZhCBHYWxheHkgUzggRzk1MEZEINiv2Ygg2LPZitmFINqp2KfYsdiq"><input id="product_brandfatitle" name="product_brandfatitle" type="hidden" value="2LPYp9mF2LPZiNmG2q8="><input id="maincategoryid" name="Product.MainCategoryId" type="hidden" value="11"><input id="productId" name="ConfigViewModel.ProductId" type="hidden" value="254007"><input id="ispreview" name="IsPreview" type="hidden" value="False">        <div class="config-right">
                                <div id="frmPnlProductConfigR_Content">

                                    <div id="productConfigDetails">
                                        <div class="c-seller">
                                            <div class="c-seller__detail">
                                                <div class="c-seller__info c-seller__info--title">
                                                    <i class="icon"></i>
                                                    فروش توسط

                                                    <span>بومرنگ</span>

                                                    <span class="c-seller__rating">
                </span>

                                                </div>
                                                <div class="c-seller__info c-seller__info--leadTime">
                                                    <i class="icon"></i>

                                                    آماده ارسال


                                                </div>
                                            </div>
                                        </div>
                                        <div id="products-price-status" class="clearfix">


                                            <div id="frmPnlTotalDiscount" class="products-price-ticket right">
                                            </div>

                                            <div id="frmPnlPayablePrice" class="products-price-finalprice right">

        <span class="label">

                قیمت
        </span>
                                                <span style="color: #4d4d4d !important;" id="frmLblPayablePriceAmount" class="price">{{$product->OutPrice}}</span>
                                                <span style="color: #4d4d4d !important;" id="frmLblPayablePriceCurrency">تومان</span>
                                            </div>
                                        </div>

                                        <form action="{{route('addToCart' ,[ 'slug' => $product->slug ])}}" id="frmAddToCartbtn" method="post">
                                            {{csrf_field()}}
                                            <input name="__RequestVerificationToken" type="hidden" value="RQWloP3bXnjdVdorLVRk_W85biUcf26CJPs4wlxybiusYQ6pw_QUaQfLEcOfFnwZUxRumpeFRlJkCbTtyxvZpdQvNNA1">
                                            <div id="products-cart-status">
                                                <div id="frmPnlAddToCart" class="dk-button-container hasIcon">
                                                    <a id="frmLbtnAddToCart" style="background-color: #a5a5a5" class="dk-button green" href="javascript:{}" onclick="$('#frmAddToCartbtn').submit();">
                                                        <i style="background-color: #a5a5a5"  class="dk-button-icon dk-button-icon-addToCart"></i>
                                                        <div style="background-color: #a5a5a5" class="dk-button-label">
                                                            <div class="dk-button-labelname">افزودن به سبد خرید</div>
                                                        </div>
                                                    </a>
                                                </div>


                                            </div>
                                            <input type="hidden" id="hifPrdId" name="productId" value="254007">
                                            <input type="hidden" id="hifColorId" name="colorId" value="1">
                                            <input type="hidden" id="hifWarrantyId" name="warrantyId" value="9">
                                            <input type="hidden" id="hifSizeId" name="sizeId">
                                            <input type="hidden" id="hifSellerId" name="sellerId" value="1">
                                        </form>




                                    </div>
                                    <div id="frmPnlProductConfigL" class="config-left">


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="dk-slogans">
                    <ul class="clearfix">
                        <li>
                            <a href="/Page/Payment-terms" class="clearfix" title="پرداخت در محل" target="_blank">
                                <i class="icon icon-payment-terms"></i><span>پرداخت در محل</span>
                            </a>
                        </li>
                        <li>
                            <a href="/Page/Delivery" class="clearfix" title="تحویل اکسپرس" target="_blank">
                                <i class="icon icon-delivery"></i><span>تحویل اکسپرس</span>
                            </a>
                        </li>
                        <li>
                            <a href="/Page/Guarantee_of_Origin" class="clearfix" title="ضمانت اصل بودن کالا" target="_blank">
                                <i class="icon icon-origin-guarantee"></i><span>ضمانت اصل بودن کالا</span>
                            </a>
                        </li>
                        <li>
                            <a href="/Page/Best-Price-Guarantee" class="clearfix" title="تضمین بهترین قیمت" target="_blank">
                                <i class="icon icon-price-guarantee"></i><span>تضمین بهترین قیمت</span>
                            </a>
                        </li>
                        <li id="ctl29_frmLI_7DaysReturnAssurance">
                            <a href="/Page/Return-Policy" class="clearfix" title="٧ روز ضمانت بازگشت" target="_blank">
                                <i class="icon icon-return-policy"></i><span>٧ روز ضمانت بازگشت</span>
                            </a>
                        </li>
                    </ul>


                </div>
            </div>
        </section>
        <article>

            <div id="frmSecProductDescription">
                <h2 class="product_seo_title">
                    <span>معرفی محصول</span>
                  {{$product->Name}}
                </h2>

                <div class="text">
                    <div class="innerContent">
                        <p>
                        </p>
                        <p>{{$product->Remark}}</p>
                        <p></p>
                    </div>
                </div>
            </div>
            <script>

                var f_l = true
                var $alsoBoughtContainer = $("#dk-also_bought-container");
                ScarabQueue.push(["recommend", {
                    logic: "ALSO_BOUGHT",
                    containerId: "dk-also_bought-container",
                    templateId: "also-bought-template",
                    limit: 35,
                    success: function (SC, render) {

                        if (SC.page.products.length && f_l) {
                            $alsoBoughtContainer.show()

                            f_l = false;
                        }

                        if (SC.page.products.length < 5) {
                            $alsoBoughtContainer.find('.scroller').removeClass('partial')
                        }

                        var $exclusiveList = $alsoBoughtContainer;
                        if ($alsoBoughtContainer.length) {
                            var exclusiveList = new CarouselList({
                                element: $exclusiveList.find('.items'),
                                isEmarsys: true,
                                disableAjaxLoad: true
                            }).initSlider(SC.page.products);
                        }

                        window.navOffset = $("div#dk-products-tabs").offset().top;

                    }
                }]);
            </script>

            <script>

                var $relatedContainer = $('#dk-emarsys-products-related');
                ScarabQueue.push(['recommend', {
                    logic: 'RELATED',
                    containerId: 'dk-emarsys-products-related',
                    limit: 35,
                    templateId: 'rec-template',
                    success: function (SC, render) {
                        if (SC.page.products.length) {
                            $relatedContainer.show(); navOffset = navOffset + 325;
                        };
                        if (SC.page.products.length < 5) {
                            $relatedContainer.find('.scroller').removeClass('partial')
                        };
                        var $relatedList = $relatedContainer;
                        if ($relatedContainer.length) {
                            var relatedList = new CarouselList({
                                element: $relatedList.find('.items'), isEmarsys: true, disableAjaxLoad: true
                            }).initSlider(SC.page.products);
                        }

                        window.navOffset = $("div#dk-products-tabs").offset().top;

                    }
                }]);

            </script>
        </article>
    </div>
</div>
</body>