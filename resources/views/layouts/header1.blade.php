<!doctype html>
<html lang="en">
<head>
    <title>
        فروشگاه اینترنتی لوازم جشن تولد
    </title>
    {{--<link rel="stylesheet" href="{{asset("css/materialize.min.css")}}">--}}
    <script src="https://www.digikala.com/Handler/PageResource.ashx" type="text/javascript"></script>
    <meta http-equiv="content-language" content="fa" />
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml?v=3" />
    <meta charset="UTF-8" />
    <meta name="ecu" content="assdfhf9817fh987f138fg98g4f98hfedj1028d018hdf28g2897g9854h80347h" />
    <meta name="ecp" content="assdfhf9817fh987f138fg98g4f98hfedj1028d018hdf28g2897g9854h80347h" />
    <meta name="author" content="فروشگاه اینترنتی دیجی کالا" />
    <meta name="language" content="fa" />
    <meta name="document-type" content="Public" />
    <meta name="document-rating" content="General" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="resource-type" content="document" />
    <meta property="place:location:latitude" content="35.7642064" />
    <meta property="place:location:longitude" content="51.4040483" />
    <meta property="business:contact_data:street_address" content="خیابان ولی‌عصر – بالاتر از میدان ونک – خیابان عطار – میدان عطار – شماره ۴۲ - ساختمان ديجی کالا" />
    <meta property="business:contact_data:locality" content="تهران" />
    <meta property="business:contact_data:country_name" content="ایران" />
    <meta property="business:contact_data:phone_number" content="+98 21 61930000" />
    <meta property="business:contact_data:website" content="http://DigiKala.com" />
    <meta property="business:contact_data:postal_code" content="3469-15875" />
    <meta property="business:contact_data:email" content="info@digikala.com" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-52DB6Z');</script><!-- End Google Tag Manager --><script> var newSearchPopup = false </script><script src="https://template.digi-kala.com/Digikala/bundles/js_jquery?v=BnUvCzNWTOddpz6utasHAkpbnQEgyy3HpiyfPSQG0Fg1"></script>
    <script src="https://template.digi-kala.com/Digikala/bundles/js_common?v=2ykbGKDb2IhX_5RjQUDkGS6nqMTaDduxr6ZfdHu6C441"></script>
    <script type='text/javascript'>var iDkConfig = new DkConfig();iDkConfig.IsCrawler = false;iDkConfig.TemplateServerUrl = 'https://template.digi-kala.com/digikala/';iDkConfig.WebPushApiUrl = 'https://webpushapi.digikala.com/';iDkConfig.IsDKNet = false;iDkConfig.DigiKalaWebApiUrl = 'https://api.digikala.com/';iDkConfig.ServiceUrl = '';iDkConfig.SearchServiceUrl = 'https://search.digikala.com/';iDkConfig.FileServerUrl = 'https://file.digi-kala.com/digikala/';iDkConfig.TvFileServerUrl = 'https://tv.digikala.com/';iDkConfig.AccountSiteUrl = 'https://accounts.digikala.com/';iDkConfig.DigiKalaMagUrl = '';iDkConfig.IsLogin= 'False';iDkConfig.AutoCompleteUrl= 'https://search.digikala.com/api/AutoComplete/';var ServiceUrl='';var ClientWebApiServiceUrl='https://api.digikala.com/';var SearchServiceUrl='https://search.digikala.com/';var FileServerUrl='https://file.digi-kala.com/digikala/';var TvFileServerUrl='https://tv.digikala.com/';var TemplateServerUrl='https://template.digi-kala.com/digikala/';var AccountSiteUrl='https://accounts.digikala.com/';</script><script src="https://template.digi-kala.com/Digikala/bundles/js_public?v=LWsGw_qSybcpEPGlMdah59AY_VUrUk4Vs5LiLIzcDVc1"></script>
    <script src="https://template.digi-kala.com/Digikala/bundles/js_xdomainrequest?v=Ic2FfCKfX6xdPI1Jw9VidPrd4w4Ij8BW7O9rAhd6b9A1"></script>
    <script src="https://template.digi-kala.com/Digikala/bundles/js_main?v=OEDYpAjAYBD0ybY5WV4L-xXCB6wPBn02ttyWivExac01"></script>
    <script src="https://template.digi-kala.com/Digikala/bundles/js_homePath?v=4SH5jlbddQzp2RhqN7PVi6VDzHpqcvEURYpTKKlCxV41"></script>
    <script src="https://template.digi-kala.com/Digikala/bundles/js_emstrack?v=ZoELSPG0SdWVAMBXGZZM03-7uWDmCv9UGrjOM4b7SI41"></script>
    <script type='text/javascript' language='JavaScript'>var currentDate = new Date()</script>
    <script type='text/javascript'>
        var ScarabQueue = ScarabQueue || [];
        (function(subdomain, id) {
            if (document.getElementById(id) == null) {
                var js = document.createElement('script');
                js.id = id;
                js.src = subdomain + '.scarabresearch.com/js/123DB8D9CCA58C7C/scarab-v2.js';
                var fs = document.getElementsByTagName('script')[0];
                fs.parentNode.insertBefore(js, fs);
            }
        })
        ('https:' == document.location.protocol ? 'https://recommender' : 'http://cdn', 'scarab-js-api');
    </script>
    <script> var userInfo = {email:''};</script>
    <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon' />
    <link rel='icon' href='/favicon.ico' type='image/x-icon' />
    <link rel='apple-touch-icon' href='/favicon.png' />
    <link href="https://template.digi-kala.com/Digikala/bundles/css_main?v=lm7XTaQTgL3zTAEsPQHRvGfRs__wbpFraaCyPMmdSpM1" rel="stylesheet"/>
    <link href="https://template.digi-kala.com/Digikala/bundles/css_HomePage?v=xir1k7vh8U8EsdD9UJmw_kH1ptTEnN8lVeXlkL9SNQM1" rel="stylesheet"/>
    <link href="https://template.digi-kala.com/Digikala/bundles/css_main?v=lm7XTaQTgL3zTAEsPQHRvGfRs__wbpFraaCyPMmdSpM1" rel="stylesheet"/>
    <link href="https://template.digi-kala.com/Digikala/bundles/css_detail_buybox?v=gqvUQMZWIzla4sMTvYOk7cQrbA4spIPFwlHkf9nl3zA1" rel="stylesheet"/>
    <link href="https://template.digi-kala.com/Digikala/bundles/css_detail_bundle?v=scYaG7PgYmAmcLl9bhGxp6gOjD3g--gdKZwyMU5yG6I1" rel="stylesheet"/>

    <link href="https://www.digikala.com/" rel="canonical" />
    <!-- ----------------- <- OPEN GRAPH SHARE TAGs -> ----------------- -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="https://template.digi-kala.com/digikala/Image/Public/vtwo/digikala-logo-slogan.png"/>
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:description" content="فروشگاه اینترنتی دیجی کالا مرجع تخصصی نقد و بررسی و فروش اینترنتی کالا از معتبرترین برندها در گروه‌‏های مختلف شامل کالای دیجیتال، لوازم خانگی، لوازم شخصی، فرهنگ و هنر و ورزش و سرگرمی با تنوعی بی‌نظیر در ایران است"/>
    <meta property="og:url" content="https://www.digikala.com//"/>
    <meta property="og:site_name" content="فروشگاه اینترنتی  دیجی کالا"/>
    <meta property="fb:app_id" content="1636917953272686" />
    <!-- -----------------  </- OPEN GRAPH SHARE TAGs -> ----------------- -->
    <!-- ----------------- <- TWITTER CARD SHARE TAGs -> ----------------- -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@Digikalacom"/>
    <meta name="twitter:title" content="فروشگاه اینترنتی  دیجی کالا"/>
    <meta name="twitter:text:description" content="فروشگاه اینترنتی دیجی کالا مرجع تخصصی نقد و بررسی و فروش اینترنتی کالا از معتبرترین برندها در گروه‌‏های مختلف شامل کالای دیجیتال، لوازم خانگی، لوازم شخصی، فرهنگ و هنر و ورزش و سرگرمی با تنوعی بی‌نظیر در ایران است"/>
    <meta name="twitter:creator" content="DigikalaCom"/>
    <meta name="twitter:image" content="https://template.digi-kala.com/digikala/Image/Public/vtwo/digikala-logo-slogan.png"/>
    <meta property="twitter:app:name:iphone" content="Digikala"/>
    <meta property="twitter:app:id:iphone" content="id1016290651"/>
    <meta property="twitter:app:id:ipad" content="id1016290651"/>
    <meta property="twitter:app:id:googleplay" content="com.digikala"/>
    <meta property="twitter:app:url:iphone" content="https://itunes.apple.com/us/app/digikala/id1016290651"/>
    <meta property="twitter:app:url:ipad" content="https://itunes.apple.com/us/app/digikala/id1016290651"/>
    <meta property="twitter:app:url:googleplay" content="https://play.google.com/store/apps/details?id=com.digikala"/>
    <meta property="twitter:app:name:googleplay" content="Digikala"/>
    <meta property="twitter:site:id" content="2348239135"/>
    <meta property="twitter:app:country" content="ir"/>
    <meta property="twitter:twitter:app:name:ipad" content="Digikala"/>
    <!-- -----------------  </- TWITTER CARD SHARE TAGs -> ----------------- -->
    <meta name="apple-itunes-app" content="app-id=1016290651, affiliate-data=myAffiliateData, app-argument=myURL" />
    <meta name="description" content="فروشگاه اینترنتی دیجی کالا مرجع تخصصی نقد و بررسی و فروش اینترنتی کالا از معتبرترین برندها در گروه‌‏های مختلف شامل کالای دیجیتال، لوازم خانگی، لوازم شخصی، فرهنگ و هنر و ورزش و سرگرمی با تنوعی بی‌نظیر در ایران است" />
    <meta name="keywords" content="فروشگاه اینترنتی, خرید آنلاین، موبایل, تبلت, لپ تاپ, تلویزیون, کامپیوتر, دوربین, کتاب,لوازم خانگی, عطر و ادکلن, فروش اینترنتی، دیجیکالا، دیجی کالا" />
    <style>
        div#dk-slider-div.slides.center footer{
            display: none;
        }
        header h3{
            color: #4d4d4d !important;
        }
        span.final-price{
            color: #4d4d4d !important;
        }
    </style>
</head>