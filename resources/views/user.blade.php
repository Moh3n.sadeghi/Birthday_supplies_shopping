@include('layouts.header')
@extends('home')
@section('content')
        <div style="margin: 0 !important;" class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a  href="/user"><i class="fa fa-dashboard fa-fw"></i> داشبورد</a>
                    </li>
                    <li>
                        <a href="{{route('Cart')}}"><i class="fa fa-shopping-cart fa-fw"></i> سبد خرید</a>
                    </li>
                    <li>
                        <a href="{{route('massage')}}"><i class="fa fa-table fa-fw"></i>پیامها</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>

    <div style="background-color: #f5f8fa !important;" id="page-wrapper">
        @yield('userContent')
    </div>
@endsection
