<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="{{asset("js/materialize.min.js")}}"></script>
    <script type="text/javascript" src="{{asset("js/jquery-3.2.1.min.js")}}"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
    <link rel="stylesheet" href="{{ asset("css/materialize.min.css") }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        .nav-30px {
            height: 30px;
            line-height: 30px;
        }

        .nav-30px  i, .nav-30px  [class^="mdi-"], .nav-30px [class*="mdi-"], .nav-30px  i.material-icons {
            height: 30px;
            line-height: 30px;
        }

        .nav-30px  .button-collapse i {
            height: 30px;
            line-height: 30px;
        }

        .nav-30px  .brand-logo {
            font-size: 1.6rem;
        }
        .container-1{
            width: 400px;
            vertical-align: middle;
            white-space: nowrap;
            position: relative;
            padding-left: 20px;
        }
        input#search{
            width: 300px;
            height: 35px;
            background: #ffffff;
            border: solid 1px #343434;
            font-size: 10pt;
            float: left;
            color: #343434;
            padding-left: 48px;
            box-sizing: content-box;
            margin-top: 12px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .container-1 .material-icons{
            position: absolute;
            top: -19px;
            margin-left: 12px;
            margin-top: 17px;
            z-index: 1;
            color: #4f5b66;
        }
        .container-1 input#search:hover, .container-1 input#search:focus, .container-1 input#search:active{
            outline:none;
            background: #ffffff;
        }
        .tabs .indicator{
            background-color: black;!important;
        }
        .dropdown-content {
            background-color: #FFFFFF;
            margin: 0;
            display: none;
            min-width: 300px; /* Changed this to accomodate content width */
            max-height: auto;
            margin-left: -1px; /* Add this to keep dropdown in line with edge of navbar */
            overflow: hidden; /* Changed this from overflow-y:auto; to overflow:hidden; */
            opacity: 0;
            position: absolute;
            white-space: nowrap;
            z-index: 9;
            will-change: width, height;
        }

        nav ul a:hover{
            background-color: transparent;
            color: #ef3f3e !important;
        }
        .dropdown-content li>a, .dropdown-content li>span{
            text-align: right;
        }
    </style>
    <title>جشن تولد</title>
</head>
<body>
         <!-- Navbar goes here -->
        <div class="row grey-lighten-5">

            <nav class="nav-extended grey lighten-5">
                <div class="nav-wrapper container">
                    <a href="#" class="brand-logo black-text">Logo</a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse black-text"><i class="material-icons">menu</i></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down black-text">
                        @if (Route::has('login'))
                            @auth
                                <li><a href="{{ url('/home') }}">Home</a></li>
                            @else
                                <li><a  data-original-title="login" data-toggle="tooltip" data-placement="top" class="btnaction black-text"><i class="material-icons right">person</i>ورود</a></li>
                                <li><a href="{{ route('register') }}" data-original-title="register" data-toggle="tooltip" data-placement="top" class="btnaction black-text"><i class="material-icons right">person_add</i>ثبت نام</a></li>
                            @endauth
                        @endif
                        <li><a href="" class="disabled black-text">ارئه خدمات جشن تولد</a></li>
                    </ul>
                    <ul class="side-nav" id="mobile-demo">
                        <li><a href="" class="black-text right-align hover-color"><i class="material-icons right" style="margin: 0 0 0 8px;">person</i>ورود</a></li>
                        <li><a href="" class="black-text right-align hover-color"><i class="material-icons right" style="margin: 0 0 0 8px;">person_add</i>ثبت نام</a></li>
                        <li><a href="" class="black-text right-align hover-color"><i class="material-icons right" style="margin: 0 0 0 8px;">add_shopping_cart</i>سبد خرید</a></li>
                        <li><div class="divider"></div></li>
                        <li><a class="subheader right">لیست دسته بندی محصولات</a></li>
                    </ul>
                </div>
                <div class="nav-content hide-on-med-and-down">
                    <div class="nav-wrapper container">
                        <a class="waves-effect waves-light btn right" style="margin-top: 13px;    padding-left: 6px; "><i style="position: absolute; top: -13px;" class="material-icons">add_shopping_cart</i></a>
                        <a class="waves-effect waves-light btn right" style="margin-top: 13px ">سبد خرید</a>
                        <form>
                            <div class="container-1 box right" >
                                <input style="direction: rtl; padding-right: 20px" id="search" type="search" placeholder="کالا یا دسته بندی مورد نظر ... " required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
            <nav class="nav-30px hide-on-med-and-down grey lighten-3" >
                <div class="nav-wrapper container">
                    <ul id="dropdown1" class="right dropdown-content">
                        <li ><a href="#">one</a></li>
                        <li><a href="#">two</a></li>
                        <li class="divider"></li>
                        <li><a href="#">three</a></li>
                    </ul>
                    <ul class="right">
                        <!-- Dropdown Trigger -->
                        <li><a class="dropdown-button black-text" href="#" data-activates="dropdown1">Dropdown<i class="material-icons left">arrow_drop_down</i></a></li>
                        <!-- Dropdown Structure -->
                        <li><a class="dropdown-button black-text" href="#" data-activates="dropdown1">Dropdown<i class="material-icons left">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-button black-text" href="#" data-activates="dropdown1">Dropdown<i class="material-icons left">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-button black-text" href="#" data-activates="dropdown1">Dropdown<i class="material-icons left">arrow_drop_down</i></a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- Page Layout here -->
        <div class="col s12 m12 l12">
            <div class="col s12 m12 l12">
				<h3>ورود به حساب کاربری</h3>
				<div class="col s12 m6 l6">
					<label>نام کاربری</label>
					<input class="form-control" type="text" placeholder="لطفا نام کاربری خود را وارد کنید" name="username">
				</div>
				<div class="col s12 m6 l6">
					<label>رمز ورود</label>
					<input class="form-control" type="password" placeholder="لطفا رمز خود را وارد کنید" name="password">
				</div>
				<div class="col s12 m12 l12">
					<button class="btn btn-success">ورود</button>
				</div>
			</div>
        </div>
        {{--scripts--}}
        <script>
  
      
        </script>
</body>
</html>