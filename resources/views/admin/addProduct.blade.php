@extends('admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">اضافه کردن محصول</h3>
        </div>
        <div class="panel-body">
            <div class="container">
                <form action="{{route('addingProduct')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">نام محصول</label>
                            <input type="text" class="form-control" name="name" id="inputEmail4"
                                   placeholder="نام محصول">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">توضیحات اجمالی</label>
                            <input type="text" name="description" class="form-control" id="inputPassword4"
                                   placeholder="توضیحات اجمالی">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">فروشنده</label>
                            <input type="text" name="sell" class="form-control" id="inputCity">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">دسته بندی</label>
                            <select name="categoryid" id="inputState" class="form-control">
                                <option selected>انتخاب دسته بندی...</option>
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->CategoryName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputCity">قیمت خرید</label>
                            <input type="text" name="inprice" class="form-control" id="inputCity">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">قیمت فروش</label>
                            <input type="text" name="outprice" class="form-control" id="inputZip">
                        </div>
                    </div>
                    <br>
                    <div class="col-11">
                        <div class="form-group">
                            <label for="inputAddress">توضیحات کامل</label>
                            <textarea type="text" name="Remark" class="form-control" id="inputAddress" placeholder="توضیحات کامل"
                                      rows="7"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">اضافه کردن محصول</button>
                </form>
            </div>
        </div>
    </div>
@endsection