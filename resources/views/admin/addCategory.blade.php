@extends('admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">اضافه کردن دسته بندی</h3>
        </div>
        <div class="panel-body">
            <div class="container">
                <form action="{{route('addingCategory')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">نام دسته بندی</label>
                            <input type="text" class="form-control" name="name" id="inputEmail4"
                                   placeholder="نام دسته بندی">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">توضیحات</label>
                            <input type="text" name="description" class="form-control" id="inputPassword4"
                                   placeholder="توضیحات اجمالی">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputState">دسته بندی</label>
                            <select name="categoryid" id="inputState" class="form-control">
                                <option value="0" selected>دسته بندی اصلی</option>
                               @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->CategoryName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="clearfix"></div>
                    <button  style="margin-right: 15px" type="submit" class="btn btn-primary">اضافه کردن دسته بندی</button>
                </form>
            </div>
        </div>
    </div>
@endsection
