@extends('admin')
@section('content')
    @foreach($Massages as $Massage)
    <div class="chat-panel panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-comments fa-fw"></i> پیامها
            <div class="btn-group pull-left">
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-chevron-down"></i>
                </button>
                <ul class="dropdown-menu slidedown pull-left">
                    <li>
                        <a href="#">
                            <i class="fa fa-refresh fa-fw"></i> Refresh
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-check-circle fa-fw"></i> Available
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-times fa-fw"></i> Busy
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-clock-o fa-fw"></i> Away
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <i class="fa fa-sign-out fa-fw"></i> Sign Out
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <ul class="chat">

                    <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">
                                    </span>
                        <div class="chat-body clearfix">
                            <div class="header">
                                <strong class="primary-font">{{\Illuminate\Support\Facades\Auth::user()->name}}</strong>
                                <small class="pull-right text-muted">
                                    <i class="fa fa-clock-o fa-fw"></i> 12 mins ago
                                </small>
                            </div>
                            <p>
                                {{$Massage->Massage}}
                            </p>
                        </div>
                    </li>
                @if ($Massage->Answer)
                <li class="right clearfix">
                                    <span class="chat-img pull-right">
                                        <img src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                             class="img-circle">
                                    </span>
                    <div class="chat-body clearfix">
                        <div class="header">
                            <small class=" text-muted">
                                <i class="fa fa-clock-o fa-fw"></i> 13 mins ago
                            </small>
                            <strong class="pull-right primary-font">Admin</strong>
                        </div>
                        <p>
                            {{$Massage->Answer}}
                        </p>
                    </div>
                </li>
                @endif
            </ul>
        </div>
        <!-- /.panel-body -->
        @if (! $Massage->Answer)
        <div class="panel-footer">
            <form action="{{route('Answer')}}">
                <div class="input-group">
                    <input name="massage" id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here...">
                    <input type="hidden" name="MassageID" value="{{$Massage->id}}">
                    <span class="input-group-btn">
                                  <button type="submit" class="btn btn-warning btn-sm" id="btn-chat">
                                        Send
                                       </button>
                                </span>
                </div>
            </form>
        </div>
        @endif
        <!-- /.panel-footer -->
    </div>
    @endforeach
    <div style="text-align:center;">
        {!! $Massages->render() !!}
    </div>
@endsection