@extends('user')
@section('userContent')
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">سبد خرید</div>
        <div class="panel-body">
            @if(!$products)
                <p>سبد خرید شما خالی است</p>
            @endif
        </div>

        <!-- Table -->
        @if($products)
            <div class="container">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام محصول</th>
                        <th>تعداد</th>
                        <th>قیمت واحد</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 0 ?>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{++$counter}}</th>
                            <td>{{$product->ProductName}}</td>
                            <td>{{$product->Number}}</td>
                            <td>{{$product->Price}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        <div class="container">
            <form action="{{route('CompleteCart')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" value="{{}}">
                <button style="margin: 0 auto;" type="submit" class="btn btn-primary">پرداخت سبد خرید</button>
            </form>
        </div>
    </div>
    @endif



@endsection