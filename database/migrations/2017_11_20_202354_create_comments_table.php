<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Title',200);//Title of the comment
            $table->string('Body',200);//The detailed content of the comment
            //$table->dateTime('Date');//The time the comment was created
            $table->integer('ProductID');//Identifying the Product ID that the comment belongs to. Foreign Key.
            $table->integer('UserID');//Identifying the User ID that makes the comment. Foreign Key.
            $table->timestamps();//The time the comment was created And Update
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
