<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');//Primary Key identifying the record
            $table->string('Name',200);//The product name
            $table->integer('CategoryID');//Foreign Key, referring to the Category ID that the product belongs to
            $table->text('Description');//Detailed description for the product
            $table->string('Sell',200);//The name of the supplier
            $table->dateTime('CreateDate');//Manufacturing datetime
            $table->string('Unit',50);
            $table->string('slug');//For URL Friendly
            $table->integer('Quantity');//Current quantity of this kind of product
            $table->integer('Upper');//The maximum stock quantity of this kind of product
            $table->integer('Lower');//The minimum stock quantity of this kind of product
            $table->integer('InPrice');//Stock price
            $table->integer('OutPrice');//Sale price
            $table->integer('PictureID');//Identifying the related Picture ID of the product. Foreign Key.
            $table->text('Remark');// Memo info for the product
            $table->timestamps();//Created At AND Update At
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
