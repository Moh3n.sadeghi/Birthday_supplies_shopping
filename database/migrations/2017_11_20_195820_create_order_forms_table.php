<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userID');//Identifying the UserID that places the order. Foreign Key.
            $table->integer('TotalNumber');//The total number of the product to buy
            $table->integer('TotalMoney');//The total fee of the product to buy
            $table->dateTime('OrderDate');//The datetime when the order is placed
            $table->integer('State');//Identifying the status of the order
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_forms');
    }
}
