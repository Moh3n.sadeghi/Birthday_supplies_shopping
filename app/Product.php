<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //protected $table = 'product';//other name for table
    /*public static function lastProducts(){
        return static::where('CreateDate' , '<' , Carbon::now())->latest()->get();
    }*/
    protected $guarded=[];
    public function scopeLastProducts($query, $take = 1){
        return $query->latest()->take($take)->get();
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
