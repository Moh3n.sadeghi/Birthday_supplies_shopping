<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Faker\Guesser\Name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index(){
        $products = \App\Product::lastProducts(3);
        return view('index', compact('products'));
    }

    public function show(Product $product){
        return view('singles',compact('product'));
    }

    public function addProduct(){
        $categories = \App\Category::where('ParentID',0)->get();
        return view('admin.addProduct',compact('categories'));
    }

    public function addingProduct(Request $request){
        $product = Product::create([
           'Name' => request('name'),
           'categoryID' => request('categoryid'),
           'Sell' => request('sell'),
           'Description' => request('description'),
           'slug' =>         request('name'),
           'InPrice' => request('inprice'),
           'OutPrice' => request('outprice'),
           'Remark' => request('Remark'),
        ]);
        return redirect('/admin');
    }

}
