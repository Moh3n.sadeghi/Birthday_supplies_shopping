<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class categorycontroller extends Controller
{
    public function index(){

    }
    public function addCategory(){
        $categories = \App\Category::where('ParentID',0)->get();
        return view('admin.addCategory',compact('categories'));
    }

    public function addingCategory(Request $request){
        dd($request);
        Category::create([
           'CategoryName' =>request('name'),
           'Remark' =>request('description'),
           'ParentID' =>request('categoryid'),
        ]);
        return redirect('/admin');
    }
}
