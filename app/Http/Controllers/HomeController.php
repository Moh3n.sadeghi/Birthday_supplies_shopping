<?php

namespace App\Http\Controllers;

use App\Massage;
use App\OrderForm;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user');
    }

    public function Cart(){
        $orderform = OrderForm::where('userID' , Auth::user()->id)->where('State',0)->find(1);
        if ($orderform) {
            $products = OrderItem::where('OrderFormID', $orderform['id'])->get();
            return view('user.cart', compact('products'));
        }
        else{
            $products = null;
            return view('user.cart', compact('products'));
        }
    }

    public function massage(){
        $Massages = Massage::where('userID',Auth::user()->id)->get();
        return view('user.massage',compact('Massages'));
    }

    public function addToCart(product $product){
        $orderform = OrderForm::where('userID' , Auth::user()->id)->where('State',0)->get();
        if (!count($orderform)){
            //Empty
        }

        if ($orderform) {
            $products = OrderItem::where('OrderFormID', $orderform['id'])->where('ProductID',$product->id)->find(1);
            if ($products) {
                OrderItem::where('OrderFormID', $orderform['id'])->where('ProductID',$product->id)
                    ->update(['Number' => $products->number]);
            }
            else{
                OrderItem::create([
                   'ProductID' => $product->id,
                   'ProductName' => $product->Name,
                   'OrderFormID' => $orderform['id'],
                    'Number' => 1,
                    'Price' => $product->OutPrice
                ]);
            }
        }
        else{
            OrderForm::create([
                'userID' => Auth::user()->id,
                'TotalNumber' => 1,
                'TotalMoney' => $product->OutPrice,
                'State' => 0
            ]);
            $orderform = OrderForm::where('userID' , Auth::user()->id)->where('State',0)->find(1);
            $products = OrderItem::where('OrderFormID', $orderform['id'])->where('ProductID',$product->id)->find(1);
            OrderItem::where('OrderFormID', $orderform['id'])->where('ProductID',$product->id)
                ->update(['Number' => $products["Number"]]);
        }
        return redirect('/user/Cart');
    }

    public function massages(){
        $Massages = Massage::paginate(2);
        return view('admin.massages',compact('Massages'));
    }

    public function Answer(Request $request){
        Massage::where('id',$request->MassageID)->update([
            'Answer' => $request->massage
        ]);
        return redirect('/admin/massages');
    }

    public function CompleteCart(){
        OrderForm::where('userID' , Auth::user()->id)->where('State',0)->update([
            'State' => 1
        ]);
        return redirect('/user/Cart');
    }
}
