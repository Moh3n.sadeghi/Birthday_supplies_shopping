<?php

namespace App\Http\Controllers;

use App\Massage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MassageController extends Controller
{
    //
    public function addingMassage(Request $request){
        $id = Auth::user()->id;
        Massage::create([
           'userID' => $id,
           'Massage' => $request->massage
        ]);
        return redirect('/user/massage');
    }
}
