# Birthday Party Scenarios
## Scenario of customer use of site shopping facilities

* __purpose__ : A scenario that determines how to use site shopping 
* __persons__ : organizers birthday party birthday party administrators who are members of the
* __Equipment__ : Web Browser
* __Scenario__ :
    1. the user enters the site in browser
    2. the home page a list of availible products in displayed
    3. the user most log into his account to purchase any of the product
    4. the customercan see his cart at the dashboard screen
    5. check the cart
    6. Click on to continue to purchase your pre-invoice
    7. Continue shopping to view and approve your profile
    8. Enter the bank system for payment
    9. After confirmation of payment, the product is sent to the confirmed address

## Customer use scenario of consulting services:

* __purpose__ : A scenario that determines how to use site consulting services
* __persons__ : organizers birthday party birthday party administrators who are members of the
* __Equipment__ : Web Browser
* __Scenario__ :
  1. The customer logs in to his account
  2. In the dashboard, you click the Contact Relations button
  3. On the page that opens, you can request a description
  4. After sending the consultants, they see the message in their inbox
  5. If you want to respond or reject
  6. If responding or rejecting the customer can see the answer on the same page as well as in their messages

## Scenario using full ceremonial service:

* __purpose__ : A scenario that determines how to use site ceremonial service
* __persons__ : organizers birthday party birthday party administrators who are members of the
* __Equipment__ : Web Browser
* __Scenario__ :
  1. The customer logs in to the site's admin page
  2. On this page, you can enter your message and submit your request
  3. In the box, a brief description of the manner in which the holding is organized, and the required equipment also provides the budget and sends the message
  4. Administrators see and can see this message in their messages
  5. If you would like to refer to this customer, the link to the customer relationship will be redirected to the message
  6. In the customer relationship section, the customer is contacted and the full information is taken from him
  7. If the request is approved, at the previous stage, complete information is sent to the staff
  8. The equipment is being prepared
  9. On the set date, the venue will be ready

** It should be noted that the total estimated cost is deposited into the account of the company so that, if the project is canceled, the amount of damages will be estimated and the customer will be refunded **
