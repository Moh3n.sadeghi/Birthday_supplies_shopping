
# UseCase Diagrams

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/UseCase.PNG)


## Phase 1

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F1UseCase.PNG)

## phase 2

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F2UseCase.PNG)

## phase 3

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F3UseCase.PNG)

## phase 4

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F4UseCase.PNG)

## phase 5

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F5UseCase.PNG)

## phase 6

![Phase1UseCase]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F6UseCase.PNG)
