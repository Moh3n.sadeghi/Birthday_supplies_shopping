# Activity Diagrams

## phase 1

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F1Activity.PNG)

## phase 2

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F2Activity.PNG)

## phase 3

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F3Activity.PNG)


## phase 4

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F4Activity.PNG)

## phase 5

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F5Activity.PNG)

## phase 6

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F6Activity.PNG)

## phase 7

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F7Activity.PNG)

## phase 8

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F8Activity.PNG)

## phase 10

![Phase1Activity]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F10Activity.PNG)
