# Sequence Diagrams

## phase 1

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F1Sequence.PNG)

## phase 2

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F2Sequence.PNG)

## phase 3

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F3Sequence.PNG)

## phase 4

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F4Sequence.PNG)

## phase 5

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F5Sequence.PNG)

## phase 6

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F6Sequence.PNG)

## phase 7

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F7Sequence.PNG)

## phase 8

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F8Sequence.PNG)

## phase 10

![Phase1Sequence]( https://gitlab.com/Moh3n.sadeghi/Birthday_supplies_shopping/blob/master/Documentation/images/F10Sequence.PNG)